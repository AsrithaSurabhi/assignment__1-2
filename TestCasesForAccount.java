import static org.junit.Assert.*;

import org.junit.Test;

public class TestCasesForAccount {
	
	@Test
	public void TestInitialBalance() {
	//Test to see All new accounts are initialized with 0 dollars in the account
	Account a=new Account(100);
	assertEquals(0,a.balance());
	}

	@Test
	public void TestWithdraw() {
  //Test to see if you can  withdraw money from an account and balance is updated immediately 
	Account a=new Account(100);
	a.withdraw(90);
	assertEquals(10,a.balance());
	}
}
	

